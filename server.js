var serverAddress = "http://104.236.158.229:3000";
var debugAddress = "http://localhost:3000";
var socket = require('socket.io-client')(serverAddress);

//var usb = require('usb');
var five = require("johnny-five");

socket.on('connect', function(){
  console.log("Connected to relay");
});

socket.on('openDoor', function(data) {
  console.log("Should open door");
  // Close door
  try {
	  if(board) {
	  	console.log("Opening door");
	  	board.digitalWrite(13, 1);
	  	board.digitalWrite(10,1);
	  	setTimeout(function() {
	  		board.digitalWrite(13, 0);
	  		board.digitalWrite(10,0);
	  		socket.emit("success");
	  	}, 1000);
	  } else {
	  	throw new Error();
	  }
	}catch(error) {
		console.log("Returning failure");
		socket.emit("failure");
	}
});

socket.on('disconnect', function(){
  console.log("Disconnected from relay");
});

// var devices = usb.getDeviceList();
// var arduino = null;
// for(int i = 0; i < devices.size; i++) {
// 	devices[i].open();
// 	devices[i].getStringDescriptor(1, function(error, data) {
// 		if(data && data.indexOf("Arduino") != -1) {
// 			arduino = devices[i];
// 			break;
// 		} else {
// 			devices[i].close();
// 		}
// 	});
// }

// The board's pins will not be accessible until
// the board has reported that it is ready
var board = five.Board()
board.on("ready", function() {

  // Set pin 13 to OUTPUT mode
  this.pinMode(13, 1);
  this.pinMode(10,1);

});
